<?php

/**
 * @file
 * uw_ct_homepage_blocks.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function uw_ct_homepage_blocks_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-home_page_blocks-field_blocks'.
  $field_instances['node-home_page_blocks-field_blocks'] = array(
    'bundle' => 'home_page_blocks',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 0,
      ),
      'embedded' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'entity_teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_blocks',
    'label' => 'Blocks',
    'required' => 0,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'call_to_action' => -1,
        'eff_highlighted_fact' => -1,
        'home_page_blocks' => -1,
        'home_page_copy_block' => 'home_page_copy_block',
        'home_page_marketing_block' => 'home_page_marketing_block',
        'home_page_marketing_item' => -1,
      ),
      'bundle_weights' => array(
        'call_to_action' => 2,
        'eff_highlighted_fact' => 3,
        'home_page_blocks' => 4,
        'home_page_copy_block' => 5,
        'home_page_marketing_block' => 11,
        'home_page_marketing_item' => 12,
      ),
      'default_edit_mode' => 'open',
      'entity_translation_sync' => FALSE,
      'title' => 'Block',
      'title_multiple' => 'Blocks',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 41,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-home_page_copy_block-field_hpb_block_title'.
  $field_instances['paragraphs_item-home_page_copy_block-field_hpb_block_title'] = array(
    'bundle' => 'home_page_copy_block',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_hpb_block_title',
    'label' => 'Block title',
    'required' => 1,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'alerts' => 'alerts',
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
          'single_page_remote_events' => 'single_page_remote_events',
          'uw_tf_basic' => 'uw_tf_basic',
          'uw_tf_comment' => 'uw_tf_comment',
          'uw_tf_conference' => 'uw_tf_conference',
          'uw_tf_contact' => 'uw_tf_contact',
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
          'uw_tf_standard_site_footer' => 'uw_tf_standard_site_footer',
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'alerts' => array(
              'weight' => -7,
            ),
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'single_page_remote_events' => array(
              'weight' => 0,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_conference' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-home_page_copy_block-field_hpb_display_title'.
  $field_instances['paragraphs_item-home_page_copy_block-field_hpb_display_title'] = array(
    'bundle' => 'home_page_copy_block',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 4,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_hpb_display_title',
    'label' => 'Display title on page',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-home_page_copy_block-field_hpcb_copy'.
  $field_instances['paragraphs_item-home_page_copy_block-field_hpcb_copy'] = array(
    'bundle' => 'home_page_copy_block',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_hpcb_copy',
    'label' => 'Copy',
    'required' => 1,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'alerts' => 0,
          'full_html' => 'full_html',
          'plain_text' => 0,
          'single_page_remote_events' => 0,
          'uw_tf_basic' => 0,
          'uw_tf_comment' => 0,
          'uw_tf_conference' => 0,
          'uw_tf_contact' => 0,
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 0,
          'uw_tf_standard_site_footer' => 0,
          'uw_tf_standard_wide' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'alerts' => array(
              'weight' => -7,
            ),
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'single_page_remote_events' => array(
              'weight' => 0,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_conference' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 15,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-home_page_marketing_block-field_hpb_block_title'.
  $field_instances['paragraphs_item-home_page_marketing_block-field_hpb_block_title'] = array(
    'bundle' => 'home_page_marketing_block',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_hpb_block_title',
    'label' => 'Block title',
    'required' => 0,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'alerts' => 'alerts',
          'full_html' => 'full_html',
          'plain_text' => 'plain_text',
          'single_page_remote_events' => 'single_page_remote_events',
          'uw_tf_basic' => 'uw_tf_basic',
          'uw_tf_comment' => 'uw_tf_comment',
          'uw_tf_conference' => 'uw_tf_conference',
          'uw_tf_contact' => 'uw_tf_contact',
          'uw_tf_standard' => 'uw_tf_standard',
          'uw_tf_standard_sidebar' => 'uw_tf_standard_sidebar',
          'uw_tf_standard_site_footer' => 'uw_tf_standard_site_footer',
          'uw_tf_standard_wide' => 'uw_tf_standard_wide',
        ),
        'allowed_formats_toggle' => 0,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'alerts' => array(
              'weight' => -7,
            ),
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'single_page_remote_events' => array(
              'weight' => 0,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_conference' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-home_page_marketing_block-field_hpb_display_title'.
  $field_instances['paragraphs_item-home_page_marketing_block-field_hpb_display_title'] = array(
    'bundle' => 'home_page_marketing_block',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_hpb_display_title',
    'label' => 'Display title on page',
    'required' => 0,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-home_page_marketing_block-field_hpb_marketing_items'.
  $field_instances['paragraphs_item-home_page_marketing_block-field_hpb_marketing_items'] = array(
    'bundle' => 'home_page_marketing_block',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 2,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_hpb_marketing_items',
    'label' => 'Marketing items',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'call_to_action' => -1,
        'eff_highlighted_fact' => -1,
        'home_page_blocks' => -1,
        'home_page_copy_block' => -1,
        'home_page_marketing_block' => -1,
        'home_page_marketing_item' => 'home_page_marketing_item',
      ),
      'bundle_weights' => array(
        'call_to_action' => 2,
        'eff_highlighted_fact' => 3,
        'home_page_blocks' => 4,
        'home_page_copy_block' => 5,
        'home_page_marketing_block' => 6,
        'home_page_marketing_item' => 7,
      ),
      'default_edit_mode' => 'open',
      'entity_translation_sync' => FALSE,
      'title' => 'Marketing item',
      'title_multiple' => 'Marketing items',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-home_page_marketing_item-field_hpb_background_image'.
  $field_instances['paragraphs_item-home_page_marketing_item-field_hpb_background_image'] = array(
    'bundle' => 'home_page_marketing_item',
    'deleted' => 0,
    'description' => 'Background images should not contain text.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_hpb_background_image',
    'label' => 'Background image',
    'required' => 1,
    'settings' => array(
      'alt_field' => 1,
      'alt_field_required' => 1,
      'default_image' => 0,
      'entity_translation_sync' => FALSE,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'focal_point' => 0,
      'image_field_caption' => array(
        'enabled' => 0,
      ),
      'image_field_caption_wrapper' => array(
        'image_field_caption_default' => array(
          'format' => 'uw_tf_standard',
          'value' => '',
        ),
      ),
      'max_filesize' => '',
      'max_resolution' => '2000x2000',
      'min_resolution' => '1000x400',
      'title_field' => 0,
      'title_field_required' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'filefield_sources' => array(
          'filefield_sources' => array(
            'attach' => 0,
            'clipboard' => 0,
            'imce' => 0,
            'reference' => 0,
            'remote' => 0,
            'upload' => 'upload',
          ),
          'source_attach' => array(
            'absolute' => 0,
            'attach_mode' => 'move',
            'path' => 'file_attach',
          ),
          'source_imce' => array(
            'imce_mode' => 0,
          ),
          'source_reference' => array(
            'autocomplete' => 0,
            'search_all_fields' => 0,
          ),
        ),
        'insert' => 0,
        'insert_absolute' => 0,
        'insert_class' => '',
        'insert_default' => 'auto',
        'insert_styles' => array(
          'auto' => 'auto',
          'colorbox__body-500px-wide' => 0,
          'colorbox__field-slideshow-slide' => 0,
          'colorbox__focal_point_preview' => 0,
          'colorbox__homepage_audience_panel' => 0,
          'colorbox__homepage_audience_panel_large' => 0,
          'colorbox__homepage_feature_stories' => 0,
          'colorbox__homepage_feature_stories_large' => 0,
          'colorbox__homepage_feature_stories_medium' => 0,
          'colorbox__homepage_feature_stories_small' => 0,
          'colorbox__homepage_feature_stories_xl' => 0,
          'colorbox__homepage_news' => 0,
          'colorbox__homepage_positioning_stories_big' => 0,
          'colorbox__homepage_positioning_stories_small' => 0,
          'colorbox__image_gallery_squares' => 0,
          'colorbox__large' => 0,
          'colorbox__medium' => 0,
          'colorbox__person-profile-list' => 0,
          'colorbox__profile-photo-block' => 0,
          'colorbox__sidebar-220px-wide' => 0,
          'colorbox__thumbnail' => 0,
          'colorbox__wide-body-750px-wide' => 0,
          'icon_link' => 0,
          'image' => 0,
          'image_body-500px-wide' => 0,
          'image_field-slideshow-slide' => 0,
          'image_focal_point_preview' => 0,
          'image_homepage_audience_panel' => 0,
          'image_homepage_audience_panel_large' => 0,
          'image_homepage_feature_stories' => 0,
          'image_homepage_feature_stories_large' => 0,
          'image_homepage_feature_stories_medium' => 0,
          'image_homepage_feature_stories_small' => 0,
          'image_homepage_feature_stories_xl' => 0,
          'image_homepage_news' => 0,
          'image_homepage_positioning_stories_big' => 0,
          'image_homepage_positioning_stories_small' => 0,
          'image_image_gallery_squares' => 0,
          'image_large' => 0,
          'image_medium' => 0,
          'image_person-profile-list' => 0,
          'image_profile-photo-block' => 0,
          'image_sidebar-220px-wide' => 0,
          'image_thumbnail' => 0,
          'image_wide-body-750px-wide' => 0,
          'link' => 0,
        ),
        'insert_width' => '',
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-home_page_marketing_item-field_hpb_marketing_item_content'.
  $field_instances['paragraphs_item-home_page_marketing_item-field_hpb_marketing_item_content'] = array(
    'bundle' => 'home_page_marketing_item',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_hpb_marketing_item_content',
    'label' => 'Marketing item content',
    'required' => 1,
    'settings' => array(
      'better_formats' => array(
        'allowed_formats' => array(
          'alerts' => 0,
          'full_html' => 0,
          'plain_text' => 0,
          'single_page_remote_events' => 0,
          'uw_tf_basic' => 0,
          'uw_tf_comment' => 0,
          'uw_tf_conference' => 0,
          'uw_tf_contact' => 0,
          'uw_tf_hpb_marketing_item' => 'uw_tf_hpb_marketing_item',
          'uw_tf_standard' => 0,
          'uw_tf_standard_sidebar' => 0,
          'uw_tf_standard_site_footer' => 0,
          'uw_tf_standard_wide' => 0,
        ),
        'allowed_formats_toggle' => 1,
        'default_order_toggle' => 0,
        'default_order_wrapper' => array(
          'formats' => array(
            'alerts' => array(
              'weight' => -7,
            ),
            'full_html' => array(
              'weight' => -6,
            ),
            'plain_text' => array(
              'weight' => 10,
            ),
            'single_page_remote_events' => array(
              'weight' => 0,
            ),
            'uw_tf_basic' => array(
              'weight' => 0,
            ),
            'uw_tf_comment' => array(
              'weight' => 0,
            ),
            'uw_tf_conference' => array(
              'weight' => 0,
            ),
            'uw_tf_contact' => array(
              'weight' => -5,
            ),
            'uw_tf_hpb_marketing_item' => array(
              'weight' => 0,
            ),
            'uw_tf_standard' => array(
              'weight' => -10,
            ),
            'uw_tf_standard_sidebar' => array(
              'weight' => -8,
            ),
            'uw_tf_standard_site_footer' => array(
              'weight' => -7,
            ),
            'uw_tf_standard_wide' => array(
              'weight' => -9,
            ),
          ),
        ),
      ),
      'entity_translation_sync' => FALSE,
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 10,
      ),
      'type' => 'text_textarea',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Background image');
  t('Background images should not contain text.');
  t('Block title');
  t('Blocks');
  t('Copy');
  t('Display title on page');
  t('Marketing item content');
  t('Marketing items');

  return $field_instances;
}
