<?php

/**
 * @file
 * uw_ct_homepage_blocks.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_homepage_blocks_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create home_page_blocks content'.
  $permissions['create home_page_blocks content'] = array(
    'name' => 'create home_page_blocks content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any home_page_blocks content'.
  $permissions['delete any home_page_blocks content'] = array(
    'name' => 'delete any home_page_blocks content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own home_page_blocks content'.
  $permissions['delete own home_page_blocks content'] = array(
    'name' => 'delete own home_page_blocks content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any home_page_blocks content'.
  $permissions['edit any home_page_blocks content'] = array(
    'name' => 'edit any home_page_blocks content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own home_page_blocks content'.
  $permissions['edit own home_page_blocks content'] = array(
    'name' => 'edit own home_page_blocks content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'use text format uw_tf_hpb_marketing_item'.
  $permissions['use text format uw_tf_hpb_marketing_item'] = array(
    'name' => 'use text format uw_tf_hpb_marketing_item',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'filter',
  );

  return $permissions;
}
