<?php

/**
 * @file
 * uw_ct_homepage_blocks.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_homepage_blocks_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_homepage_blocks_node_info() {
  $items = array(
    'home_page_blocks' => array(
      'name' => t('Home page blocks'),
      'base' => 'node_content',
      'description' => t('Places blocks onto the homepage.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_ct_homepage_blocks_paragraphs_info() {
  $items = array(
    'home_page_blocks' => array(
      'name' => 'Home page blocks',
      'bundle' => 'home_page_blocks',
      'locked' => '1',
    ),
    'home_page_copy_block' => array(
      'name' => 'Home page copy block',
      'bundle' => 'home_page_copy_block',
      'locked' => '1',
    ),
    'home_page_marketing_block' => array(
      'name' => 'Home page marketing block',
      'bundle' => 'home_page_marketing_block',
      'locked' => '1',
    ),
    'home_page_marketing_item' => array(
      'name' => 'Home page marketing item',
      'bundle' => 'home_page_marketing_item',
      'locked' => '1',
    ),
  );
  return $items;
}
