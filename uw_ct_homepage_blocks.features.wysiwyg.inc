<?php

/**
 * @file
 * uw_ct_homepage_blocks.features.wysiwyg.inc
 */

/**
 * Implements hook_wysiwyg_default_profiles().
 */
function uw_ct_homepage_blocks_wysiwyg_default_profiles() {
  $profiles = array();

  // Exported profile: uw_tf_hpb_marketing_item.
  $profiles['uw_tf_hpb_marketing_item'] = array(
    'format' => 'uw_tf_hpb_marketing_item',
    'editor' => 'ckeditor',
    'settings' => array(
      'default' => 1,
      'user_choose' => 0,
      'show_toggle' => 1,
      'add_to_summaries' => 1,
      'theme' => '',
      'language' => 'en',
      'buttons' => array(
        'default' => array(
          'Bold' => 1,
          'Italic' => 1,
          'Strike' => 1,
          'BulletedList' => 1,
          'NumberedList' => 1,
          'Undo' => 1,
          'Redo' => 1,
          'Link' => 1,
          'Unlink' => 1,
          'Anchor' => 1,
          'Superscript' => 1,
          'Subscript' => 1,
          'Blockquote' => 1,
          'Source' => 1,
          'PasteText' => 1,
          'ShowBlocks' => 1,
          'Styles' => 1,
          'SelectAll' => 1,
          'Find' => 1,
          'Maximize' => 1,
          'Scayt' => 1,
        ),
        'smallerselection' => array(
          'smallerselection' => 1,
        ),
        'uw_config' => array(
          'uw_config' => 1,
        ),
        'templates' => array(
          'Templates' => 1,
        ),
        'uw_liststyle' => array(
          'uw_liststyle' => 1,
        ),
        'codemirror' => array(
          'codemirror' => 1,
        ),
        'textselection' => array(
          'textselection' => 1,
        ),
        'drupal_path' => array(
          'Link' => 1,
        ),
      ),
      'toolbarLocation' => 'top',
      'resize_enabled' => 1,
      'default_toolbar_grouping' => 0,
      'simple_source_formatting' => 0,
      'acf_mode' => 0,
      'acf_allowed_content' => '',
      'css_setting' => 'theme',
      'css_path' => '',
      'stylesSet' => '',
      'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6,div',
      'advanced__active_tab' => 'edit-basic',
      'forcePasteAsPlainText' => 0,
    ),
    'rdf_mapping' => array(),
  );

  return $profiles;
}
